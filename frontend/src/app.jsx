import React from "react";
import ClientList from "./components/clientList"

const App = () => {


  return (
    <div className="app">
      <h1>EVPro Full-stack Test</h1>

		<ClientList />
    </div>
  );
};

export default App;
