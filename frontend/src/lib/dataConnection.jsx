const client_data = [
	{
		id:0,
		name:'Andy', 
		email:'agsystems@gmail.com', 
		createdDate:'Tue Nov 30 2021 16:10:05 GMT+0000 (GMT)',
		company:'It\'s Andy'
	},
	{
		id:1,
		name:'Mr U Surname', 
		email:'usurname@gmail.com', 
		createdDate:'Tue Nov 30 2021 15:10:05 GMT+0000 (GMT)',
		company:'Surname co.'
	}	
]

export function filterResults(query)
{
	let return_data = client_data;

	if(query)
	{
		return_data = [];
		
		client_data.forEach(client => {
			if( client.name.toLowerCase().indexOf( query.toLowerCase() )>-1 )
			{
				return_data.push(client)
			}
		});
	}

	return return_data;
}

export function addClient(clientObject)
{
	clientObject.id = client_data.length;
	clientObject.createdDate = ''+new Date();
	client_data.push( clientObject );
}