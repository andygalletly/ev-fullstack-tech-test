import React from 'react'
import { addClient } from '../lib/dataConnection'

import './addClientForm.css';

class AddClientForm extends React.Component {
	constructor(props)
	{
		super(props);
		this.defaultState = {clientName:'',clientEmail:'',clientCompany:'',enableSubmit:false};
		this.state = this.defaultState;
	
	}

	componentDidMount = () => {
		// set default state when mounting the component
		this.state = this.defaultState;
	}

	addNewClient = (e) => {
		const newCleint = {};
		newCleint.name = this.state.clientName;
		newCleint.email = this.state.clientEmail;
		newCleint.company = this.state.clientCompany;
		addClient(newCleint);
		this.props.updateFunction()
	}

	updateNewClient = (e) => {
		const name = e.target.name;
		const value = e.target.value;
		this.setState({[name]: value})

		this.checkEnableSubmit()

	}

	checkEnableSubmit = () => {
		let do_enable = true;

		if(!this.state.clientName)
		{
			do_enable = false;
		}
		if(this.state.clientEmail.indexOf('@')<0) // should really do some better checking for this
		{
			do_enable = false;
		}
		if(!this.state.clientCompany)
		{
			do_enable = false;
		}

		this.setState({enableSubmit:do_enable})
	}

	render()
	{


		return (
			<div className="addClient">
			{/* name, email, createdDate & company */}
			<h1>Add Client</h1>
			<div className="form">
				
				<input type="text" placeholder="Name" name="clientName" onChange={this.updateNewClient} value={this.state.clientName} />
				
				<input type="text" placeholder="Email" name="clientEmail" onChange={this.updateNewClient} value={this.state.clientEmail} />
				
				<input type="text" placeholder="Company" name="clientCompany" onChange={this.updateNewClient} value={this.state.clientCompany} />
				<div classNAme="buttons">
				{ this.state.enableSubmit && <button className="button" onClick={this.addNewClient}>+ ADD</button> }
				{ (!this.state.enableSubmit) && <button className="button" disabled>+ ADD</button> }
				</div>
			</div>
		</div>
		)
	}
}
export default AddClientForm;