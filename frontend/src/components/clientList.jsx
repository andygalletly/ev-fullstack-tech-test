
import React from 'react'
import { filterResults } from '../lib/dataConnection'
import AddClientForm from './addClientForm';
import './clientList.css'

class ClientList extends React.Component {
	constructor(props)
	{
		super(props);
		var _results = filterResults(null)// get results with no filter
		this.defaultState = {results:_results,return:true,searchterm:''};
		this.state = this.defaultState;
	
	}

	componentDidMount = () => {
		// set default search state when mounting the component
		var _results = filterResults(null)
		this.defaultState.results = _results;
		this.setState(this.defaultState)
	}

	updateData = () => {
		var _results = filterResults(null)
		this.defaultState.results = _results;
		this.setState(this.defaultState)
	}

	// update search terms
	// and update the results array
	changeSearch = (e) => {
		
		const search_term = e.target.value
		let newResults = filterResults(search_term);

		this.setState({results: newResults,searchterm:search_term});
	}

	// empty the search and results array
	resetSearch = () => {
		let newResults = filterResults(null);
		
		this.setState({results: newResults,searchterm:''});
	}

	formatDate = (date) => {
		const dateObj = new Date(date);
		return dateObj.getDate() + '/' + (dateObj.getMonth()+1) + '/' + dateObj.getYear();
	}

	render() {
		
		return (
			<div className="clientList">
				<div className="main">

				<div className="search"><input type="text" placeholder="Search" onChange={this.changeSearch} /></div>
				<div className="clients">
					{ this.state.results && this.state.results.map (
						(client, index) => 
						(
							<div className="client" key={index}>
								<p>Name: {client.name}</p>
								<p>Email: {client.email}</p>
								<p>Company: {client.company}</p>
								{client.createdDate && <p>Date Added: {this.formatDate(client.createdDate)}</p>}
							</div>
						)
	
					) }
					</div>
				</div>

				<AddClientForm updateFunction={this.updateData} />
			</div>
	  )
	}
  }

  export default ClientList;